class SubscribeVacancies {
    constructor() {
        this.departmentSelect();
        this.technologySelect();
        this.errorMessage();
        this.personalAccess();
    }

    personalAccess() {
        $('.c-form__personal input').on('click', function () {
            const parent = $(this).closest('.c-form__personal');
            $(parent).removeClass('error')
            }
        )
    }

    departmentSelect() {
       $('.b-subscribe-vacancies__department button').on('click', (el) => {
           const departmentId = $(el.currentTarget).data('id');

           $(el.currentTarget).siblings().removeClass('btn-primary').addClass('btn-light');
           $(el.currentTarget).removeClass('btn-light').addClass('btn-primary');

           $('.b-subscribe-vacancies__technology-list').addClass('d-none');
           $(`.b-subscribe-vacancies__technology-list[data-id="${departmentId}"]`).removeClass('d-none');
       });
   }

    technologySelect() {
        let technologiesCheck = false;

        $('.b-subscribe-vacancies__technology input').on('click', (el) => {
            const element = el.target,
                technologiesValue = $(element).siblings('label').text(),
                technologiesId = $(element).attr('id'),
                technologiesWrap = $('.b-subscribe-vacancies__subscr-list');

            $('.b-subscribe-vacancies__technology-error').addClass('d-none');

            if($(element).is(':checked')) {
                const technologiesItem = $('<div>', {"class": "b-subscribe-vacancies__subscr-item"}),
                    technologiesClear = $('<span>', {"class": "b-subscribe-vacancies__subscr-clear"});

                $('.b-subscribe-vacancies__subscr-list').append(technologiesItem);
                $(technologiesItem).text(technologiesValue);
                $(technologiesItem).attr('data-tech',technologiesId);
                $(technologiesItem).append(technologiesClear);

                technologiesCheck = true;
            } else {
                technologiesWrap ? $(technologiesWrap).find(`[data-tech='${technologiesId}']`).remove() : null

            }

            this.removeTechnology();
        })
    }

    removeTechnology() {
        $('.b-subscribe-vacancies__subscr-clear').on('click', (el) => {
            const element = el.target,
                technologyParentId = $(element).parents('.b-subscribe-vacancies__subscr-item').attr('data-tech');

                $(element).parents('.b-subscribe-vacancies__technology').find('#' + technologyParentId).prop('checked',false);
                $(element).parents('.b-subscribe-vacancies__subscr-item').remove();
        })
    }

    errorMessage() {
        $('#subscriptionButton').on('click',() => {
            const choosedTecnhologies = $('.b-subscribe-vacancies__technology').find('input:checkbox:checked'),
                personalAccess = $('.b-subscribe-vacancies__personal input'),
                personalAccCheck = $(personalAccess).prop('checked'),
                errorBlock = $(personalAccess).closest('.c-form__personal');

                !choosedTecnhologies.length ? $('.b-subscribe-vacancies__technology-error').removeClass('d-none') : null;
                !personalAccCheck ? $(errorBlock).addClass('error') : null;
        })
    }
}