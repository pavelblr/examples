class NewsAllComponent {
    constructor() {
        this.currentOffset = Number(document.getElementById('news-currentoffset').getAttribute('data-current-offset'));
        this.offset =  Number(document.getElementById('news-offset').getAttribute('data-offset'));;
        this.template = "";
        this.queryProgress = false;
        this.locale = document.getElementById('main-locale').value;

        document.querySelector('[data-action="showMoreNews"]').addEventListener('click', (evt) => {
            this.showMoreNewsClickHandler(evt);
        });
    }

    showMoreNewsClickHandler(evt) {
        this.getNewsAllAndRender(`/getAllNews?offset=${this.currentOffset}&locale=${this.locale}`);
    }

    getNewsAll(endpoint) {
        if(this.queryProgress){
            return;
        }

        const method = "GET",
            dict = { method, endpoint};

        this.queryProgress = true;

        return NewsService.getNewsAllByDepartment(dict)
            .then((resp) => {
                this.template = resp.content;
                this.currentOffset += this.offset;
                this.renderShowMoreBtn(resp.newsLeft);
                this.queryProgress = false;
            });
    }

    renderShowMoreBtn(newsLeft){
        const showMore = document.querySelector('[data-action="showMoreNews"]');

        if (!showMore) {
            return;
        }

        newsLeft > 0 ? showMore.style.display='inline' : showMore.style.display='none';
    }

    getNewsAllAndRender(endpoint) {
        this.getNewsAll(endpoint).then(() => {
            this.renderNews();
        });
    }

    renderNews(){
        const newsItemsWrap = document.getElementsByClassName('b-news-items__list')[0],
            newsItemsVirtContainer = document.createElement('div'),
            newsItems = newsItemsVirtContainer.querySelectorAll(".b-news-items__item"),
            newsItemsFragment = document.createDocumentFragment();

        newsItemsVirtContainer.innerHTML = this.template;

        for(let i = 0; i < newsItems.length; i++) {
            newsItemsFragment.appendChild(newsItems[i]);
        }

        newsItemsWrap.appendChild(newsItemsFragment);
    }
}
 